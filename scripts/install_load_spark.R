#############################################
## Install sparklyr (and local Spark)
#############################################

# install the sparklyr package
install.packages("sparklyr")

# load sparklyr and tidyverse (a collection of handy packages)
library(sparklyr)
library(tidyverse)

# check which spark installations are available locally
sparklyr::spark_available_versions()

# spark can be installed locally as follows:
spark_install(version = "3.1")

# create configuration list, e.g. change the number of local clusters and memory
# see https://spark.rstudio.com/guides/connections/
conf <- spark_config()
conf$sparklyr.cores.local = 4

# create local spark cluster
sc <- spark_connect(master = "local", version = "3.1", config = conf)

## see web interface (also via RStudio "Connections" tab)
spark_web(sc)

# disconnect from spark
spark_disconnect(sc)

